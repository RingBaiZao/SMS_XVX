# SMS_XVX

#### 介绍
{**以下是码云平台说明，您可以替换此简介**
码云是 OSCHINA 推出的基于 Git 的代码托管平台（同时支持 SVN）。专为开发者提供稳定、高效、安全的云端软件开发协作平台
无论是个人、团队、或是企业，都能够用码云实现代码托管、项目管理、协作开发。企业项目请看 [https://gitee.com/enterprises](https://gitee.com/enterprises)}

#### 软件架构
软件架构说明
function1和function3：

build内存放可执行文件，include存放头文件，source存放源文件，通过编写Makefile进行编译。

main.c为程序函数主体，其余为相关增删改查函数。

#### 使用说明

1.function1中实现功能一，使用MYDB数据库中stu_info和class_info表。
2.function3实现所有功能，使用MYDB中stu_1,sc_1,source_1表，分别代表学生信息，课程信息，和学生课程分数。
运行build内可执行文件，按照提示对MYDB库中表进行操作。
![输入图片说明](https://images.gitee.com/uploads/images/2019/1020/235208_0d305a7b_5295020.png "QQ截图20191020235148.png")

#### 参与贡献



#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)